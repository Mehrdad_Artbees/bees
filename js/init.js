var log = (function () {
    var log = "";

    return {
        add: function (msg) { log += msg + "\n"; },
        show: function () { alert(log); log = ""; }
    }
})();



var request = (function () {
    var cache = {};
    var me = this;
    function setOpts(opts){
        me.opts = {};
        me.success = me.loading = me.failed = false;
        me.errorStatus = me.errorBody = '';
        me.data = null;
        me.opts.method = opts.message || 'GET';
        me.opts.url = opts.url || '/';
        if(opts.type)me.opts.type = opts.type;
        me.opts.background = opts.background || true;
        me.opts.extract = function (xhr) {
            if (xhr.status >= 300) { // error!
                me.failed = true;
                me.success = me.loading = false;
                me.errorStatus = xhr.status;
                me.errorBody = xhr.responseText;
                m.redraw();
            }
            return xhr.responseText;
        };
        me.opts.callback = opts.callback || false;
        me.staleCache = opts.staleCache || true;
        me.opts.redraw = opts.redraw || false;
    }
    function send() {
        me.loading = true;
        me.success = me.failed = false;
        var key = JSON.stringify(me.opts);
        if( ! cache[key] ) {
            cache[key] = m.request(me.opts).then(function (data) { // success!
                me.success = true;
                me.failed = me.loading = false;
                me.data = data;
                if(me.staleCache)
                    delete cache[key];
                return data;
            });
            if (me.opts.callback)
                cache[key] = cache[key].then(me.opts.callback.success,me.opts.callback.failure);

            if (me.opts.redraw)
                cache[key].then(m.redraw);
        }
        return cache[key];

    }
    return {
        facade : function( opts ) {
            if(opts)
                setOpts(opts);
            return send();
        }
    }
})();


var bees =  {
    /*
     * usage:
     * this.usersVM = viewModelMap({
     *  isEditing: m.prop(false),
     *  tempValue: m.prop(""),
     *  error: m.prop("")
     *})
     *
     * var vm = ctrl.usersVM(user.id)
     *
     * vm.isEditing() returns false
     *
     * vm.isEditing(true) sets it to true
     */
    viewModelMap: function(signature) {
        var map = {};
        return function(key) {
            if (!map[key]) {
                map[key] = {};
                for (var prop in signature) map[key][prop] = m.prop(signature[prop]())
            }
            return map[key]
        }
    },
    filter: function( obj, predicate, result) {
        result = result || [];
        var key;
        for (key in obj) {
            if (obj.hasOwnProperty(key) && predicate(key)) {
                result[key] = obj[key];
            }
        }
        return result;
    },
    inArray: function inArray(needle, haystack) {
        // var length = haystack.length;
        // for(var i = 0; i < length; i++) {
        //     if(haystack[i] == needle) return true;
        // }
        // return false;
        return haystack.indexOf(needle) >= 0;
    },
    InvalidWidgetException: function(message){
        this.message = message || "Invalid widget!";
        this.type = "InvalidWidgetException";
    },
    container: {

    },
    resizable: function(selector,callback){
        [].forEach.call( document.querySelectorAll(selector), function(el){
            if(! el.nextSibling) return;
            el.mr=el.offsetWidth;
            el.insertAdjacentHTML("beforeend","<div class='mresize' style='width:4px;height:100%;border:0;visibility:visible;margin:0;position:absolute;top:0;bottom:0;left:" + ( el.mr - 2 ) + "px;z-index:999;cursor: col-resize;'></div>");
            [].forEach.call( el.querySelectorAll(".mresize"),function(elem){
                // Bind the functions...
                elem.onmousedown = function () {
                    (new bees.drag("h")).init(this,el,function(){
                        var me = this;
                        if(( me.x_pos - me.x_elem ) - me.parent.offsetWidth != 0) {
                            if(callback) callback.call(me);
                            me.parent.nextSibling.style.width = me.parent.nextSibling.offsetWidth - ( ( me.x_pos - me.x_elem ) - me.parent.offsetWidth ) + "px";
                            me.parent.style.width = ( me.x_pos - me.x_elem ) + "px";
                        }
                    });
                    return false;
                };
            });
        });
    },
    append: function ( dom ){
        return function( el, init ){
            if( !init ) el.appendChild( dom );
        };
    },
    drag: function(type){
        var me = this;
        me.selected = true;
        //h for horizontal
        //v for vertical
        //b for both
        me.type = type || "b";
        // Will be called when user starts dragging an element
        this.init = function(elem, parent, callback){
            window.addEventListener('mouseup', me._destroy, false);
            window.addEventListener('mousemove', me._move_elem, false);
            // Store the object of the element which needs to be moved
            me.selected = true;
            me.elem = elem;
            me.callback = callback;
            me.parent = parent;
        };

        // Will be called when user dragging an element
        this._move_elem = function(e) {
            // Stores x coordinate of the mouse pointer
            me.x_pos = document.all ? window.event.clientX : e.pageX;
            me.y_pos = document.all ? window.event.clientY : e.pageY;
            // Stores top, left values (edge) of the element
            if(! me.x_elem)
                me.x_elem = me.x_pos - me.elem.offsetLeft;
            if(! me.y_elem )
                me.y_elem = me.y_pos - me.elem.offsetTop;
            if (me.selected !== false) {
                if(me.type == "h" || me.type == "b") {
                    me.elem.style.left = (me.x_pos - me.x_elem) + 'px';
                }
                if(me.type == "v" || me.type == "b") {
                    me.elem.style.top = (me.y_pos - me.y_elem) + 'px';
                }
            }
        };

        // Destroy the object when we are done
        this._destroy = function() {
            window.removeEventListener('mouseup', me._destroy, false);
            window.removeEventListener('mousemove', me._move_elem, false);
            if(me.callback) me.callback.call(me);
            me.selected = false;
        };

    },
    execute: function (command) {
        return command.execute(command.data);
    }
};

events.subscribe('layout.get',function(){
    bees.request({url:'schema/layout.json'}).send().then(function(layout){
        events.publish('layout.ready',{layout: m.prop(layout)})
    });
});

events.subscribe('setting.get',function(){
    bees.request({url:'schema/settings.json'}).send().then(function(setting){
        // console.log(setting)
        events.publish('setting.ready',{setting: m.prop(setting)})
    });
});

events.subscribe('container.ready',function(){
    var elem = document.querySelector('.ab-container');
    bees.container = {
        elem: elem,
        width: elem.offsetWidth,
        height: elem.offsetHeight
    };
    bees.resizable('.ab-col')
});

