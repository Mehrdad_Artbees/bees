/*
*  Layout Model:
*  This model follows the Singleton pattern
* */
var Layout = (function(){

    var me = this;

    /*
    * Set to a default value; instance() is undefined
    * The instance() getter is singleton's unique instance
    */
    var instance = m.prop();

    /*
    * Set to a default value; validInstance() is undefined
    * After the layout is validated using the validator.json
    * this property is filled with valid layout object
    * The validation is done via Layout.ContainerVM
     */
    var validInstance = m.prop();

    /*
    * This object contains error messages related to bees.request
    * //TODO: check if errors reach here, since bees.request already extracts errors
    * */
    var error = m.prop();


    /*
    * Layout.init initializes a request and returns the promise
    * if the instance() is undefined
    * */
    function init(){
        return  request.facade({url: 'schema/layout.json', callback: {success: instance, failure: error}});
    }

    /*
    * Layout.basicTypes are contents of layout which are final
    * means they don't have a content
    * */
    var basicTypes = ["text"];

    /*
     * This class is Container View Model which should keep track of changes to the layout
     * //whenever VDOM changes, this object should change
     * @param data Object, this param contains the unvalidated layout
     */
    me.ContainerVM = function(data){
        //validated layout would be stored in layout object
        var layout = {};
        layout.attributes = {};
        layout.attributes.id = 'ab_container';
        layout.type = "container";
        //layout is passed by reference here
        //Validator.getInstance().container returns the validator instance for container element
        me.generateValidLayout(layout,data, Validator.getInstance().container);
        //
        if(data.hasOwnProperty('content')) {
            layout.content = me.ContentVM(
                bees.filter(data.content,
                    Validator.contentValidator
                        .bind(this,Validator.getInstance().container.content)
                        .bind(this,data.content)
                ),
                Validator.getInstance()
            );
        }
        return layout;
    };

    /*
    * This class recursively generates a View Model for any element except the container
    * */
    me.ContentVM = function(content, validator){
        var data = [];
        content.map(function(elem,idx){
            data[idx] = {};
            me.generateValidLayout(data[idx],elem, validator[elem.type]);
            data[idx].type = elem.type;
            if(elem.hasOwnProperty('content')) {
                // console.log('***********************')
                // console.log(elem)
                // console.log(content)
                data[idx].content =
                    me.ContentVM(
                        bees.filter(
                            elem.content,
                            Validator.contentValidator
                                .bind(this,validator[elem.type].content)
                                .bind(this,elem.content)
                        ), validator
                    );
            }
        });
        // console.log(data)
        return data;
    };


    /*
     * This component is responsible for drawing the container
     * alongside it's content
     */

    /*
     * this method generates a valid object from initial object
     * using the validator provided in layout.json
     */
    me.generateValidLayout = function(elem,layout,validator){
        // console.log(layout)
        // console.log(elem)
        // console.log(validator)
        if(layout.hasOwnProperty('type') && bees.inArray(layout.type,basicTypes)){
            for(var key in layout){
                if(layout.hasOwnProperty(key))
                    elem[key] = layout[key];
            }
        }
        else if(validator.hasOwnProperty('attributes')) {
            validator.attributes.map(function (attribute) {
                if (layout.hasOwnProperty(attribute.key)) {
                    if (attribute.hasOwnProperty('attributes')) {
                        elem[attribute.key] = {};
                        me.generateValidLayout(elem[attribute.key], layout[attribute.key],attribute)
                    } else {
                        elem[attribute.key] = layout[attribute.key];
                    }
                }
            })
        }
    };
    return {
        getInstance: function(){
            if( ! instance() ){
                return init();
            } if( ! validInstance() ){
                return instance();
            }
            return validInstance();
        },
        validate: function(){
            validInstance(me.ContainerVM(instance()));
        },
        basicTypes: basicTypes
    }

})();


var Validator = (function(){
    var instance = m.prop();
    var error = m.prop();
    var me = this;
    function init(){
        return request.facade({url:'schema/validator.json',callback: {success: instance,fail: error}});
    }

    return {
        getInstance: function(){
            if(! instance() ){
                return init();
            }
            return instance();
        },
        /*
         * Checks if an element can contain another
         * Example: contentValidator(validator,{row element},'row') returns false
         * contentValidator(validator, {col element},'menu') returns true
         * TODO: Might cause an issue on a condition that a container accepts more than one type
         * as an example if you could have a row and a column inside another row
         */
        contentValidator: function(validator, content,key){
            var valid = bees.inArray(key,Layout.basicTypes) || validator.accepts.hasOwnProperty(content[key].type);
            if(valid){
                if(validator.accepts[content[key].type].hasOwnProperty('max')
                    && key >= validator.accepts[content[key].type])
                    valid = false;
            }
            return valid;
        }
    }
})();