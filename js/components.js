/*
 * This is the top level component. It acts as a coordinator between other
 * components of the system.
 *
 */


//model
var coordinator = {

};



coordinator.controller = function(){
    var ctrl = this;
    ctrl.ready = m.prop(false);
    ctrl.layout = m.prop();
    ctrl.validator = m.prop();
    ctrl.init = function(){
        m.sync([Layout.getInstance(),Validator.getInstance()]).then(function(){
            Layout.validate();
            ctrl.ready(true);
            m.redraw();
        }).then(function(){
            events.publish('container.ready')
        });
    }
};

coordinator.view = function(ctrl){
    if( ! ctrl.ready() )ctrl.init();
    if(ctrl.ready()) {
        return m('.row', [
            m('.col-2', [
                m('h2','Widgets'),
                m.component(components, {widgets: bees.filter(Validator.getInstance(),function(elem){ return bees.inArray(elem,components.widgets)},{})})
            ]),
            m('.col-10', [
                m('h2','Content'),
                m.component(container, {layout: Layout.getInstance()})
            ])
        ])
    }
    else{
        return m('.loading',"Loading...");
    }
};



//components
var components = {
    widgets: ['logo','menu','row','col']
};

components.controller = function(){
    var ctrl = this;
    ctrl.drawWidgets = function(widgets){
        return m('#widget_container.widget_container',[
            components.widgets.map(function(widget_name){
                switch (widget_name){
                    case "menu":
                        return bees.execute(WidgetCommands.DrawMenu());
                    case "logo":
                        return bees.execute(WidgetCommands.DrawLogo());
                    case "row":
                        return bees.execute(WidgetCommands.DrawRow());
                    case "col":
                        return bees.execute(WidgetCommands.DrawCol());
                    default:
                        return '';
                    //TODO: default should throw exception
                }
            })
        ])
    }
};

components.view = function(ctrl,args){
    return ctrl.drawWidgets(args.widgets);
};







/*
* container component
 */

//Model
var container = {};

container.controller = function(){
    var ctrl = this;
    ctrl.content = function(elem){
        switch (elem.type){
            case "container":
                return bees.execute(BuilderCommands.DrawContainer(elem));
            case "row":
                return bees.execute(BuilderCommands.DrawRow(elem));
            case "col":
                return bees.execute(BuilderCommands.DrawCol(elem));
            case "logo":
                return bees.execute(BuilderCommands.DrawLogo(elem));
            case "menu":
                return bees.execute(BuilderCommands.DrawMenu(elem));
            default:
                // throw new bees.InvalidWidgetException("Invalid exception with type " + elem.type + " in container.controller");
                return '';
        }

    };
};
container.view = function(ctrl,args){
    return ctrl.content(args.layout)
};

var elements = {
    container: function(c){
        return m('div', c.attributes, [
            c.content.length > 0 ?
                c.content.map(function(elem){
                    return m.component(container,{layout: elem})
                }) : "Container"
        ]);
    },
    row: function(row){
        return m('.ab-row', [
            row.content.length > 0 ?
                row.content.map(function(elem){
                    return m.component(container,{layout: elem})
                }) : "Row#" + row.attributes.id
        ])
    },
    column: function(col){
        return m('.ab-col', col.attributes, [
            col.content.length > 0 ?
                col.content.map(function(elem){
                    return m.component(container,{layout: elem})
                }) : "Column#" + col.attributes.id
        ])
    },

    logo: function(logo){
        return m('.ab-logo', logo.attributes, [
            m('.widget',[
                "Logo"
            ])
        ])
    },


    menu: function(menu){
        return m('.ab-menu', menu.attributes, [
            "Menu"
        ])
    }
};

var widgets = {
    menu: function () {
        return m('.widget',[
            m('h3',"Menu Element")
        ])
    },
    logo: function () {
        return m('.widget',[
            m('h3',"Logo Element")
        ])
    },
    row: function () {
        return m('.widget',[
            m('h3',"Row Element")
        ])
    },
    col: function () {
        return m('.widget',[
            m('h3',"Column Element")
        ])
    }
}
var Command = function(execute,data){
    this.execute = execute;
    this.data = data;
};

var WidgetCommands = {
    DrawMenu: function(){
        return new Command(widgets.menu);
    },
    DrawLogo: function(){
        return new Command(widgets.logo);
    },
    DrawRow: function(){
        return new Command(widgets.row);
    },
    DrawCol: function(){
        return new Command(widgets.col)
    }
}

var BuilderCommands = {
    DrawContainer : function(layout){
        return new Command(elements.container,layout);
    },

    DrawRow : function(layout){
        return new Command(elements.row,layout);
    },

    DrawCol : function(layout){
        return new Command(elements.column,layout);
    },

    DrawLogo : function(layout){
        return new Command(elements.logo,layout);
    },

    DrawMenu : function(layout){
        return new Command(elements.menu,layout);
    }

}



